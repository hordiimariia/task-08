package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	List<Flower> flowerList = new ArrayList<>();
	Flower currentFlower;
	Flower.VisualParameters currentVisParam;
	Flower.GrowingTips currentGrowTips;
	private StringBuilder currentValue = new StringBuilder();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getFlowerList() {
		return flowerList;
	}

	@Override
	public void startDocument(){
		flowerList = new ArrayList<>();
	}

	@Override
	public void endDocument() throws SAXException {
		super.endDocument();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		currentValue.setLength(0);

		if (qName.equalsIgnoreCase("flower")) {
			currentFlower = new Flower();
			currentVisParam = currentFlower.new VisualParameters();
			currentGrowTips = currentFlower.new GrowingTips();

			currentFlower.setVisualParameters(currentVisParam);
			currentFlower.setGrowingTips(currentGrowTips);
		}
		if (qName.equalsIgnoreCase("aveLenFlower")) {
			// get tag's attribute by name
			String lenMeasure = attributes.getValue("measure");
			currentVisParam.setAveLenFlowerMeasure(lenMeasure);
		}

		if (qName.equalsIgnoreCase("tempreture")) {
			// get tag's attribute by index, 0 = first attribute
			String tempMeasure = attributes.getValue(0);
			currentGrowTips.setTempretureMeasure(tempMeasure);
		}

		if (qName.equalsIgnoreCase("lighting")) {
			// get tag's attribute by index, 0 = first attribute
			String lightRequiring = attributes.getValue(0);
			currentGrowTips.setLightRequiring(lightRequiring);
		}

		if (qName.equalsIgnoreCase("watering")) {
			// get tag's attribute by index, 0 = first attribute
			String waterMeasure = attributes.getValue(0);
			currentGrowTips.setWateringMeasure(waterMeasure);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {

		if (qName.equalsIgnoreCase("name")) {
			currentFlower.setName(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("soil")) {
			currentFlower.setSoil(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("origin")) {
			currentFlower.setOrigin(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("multiplying")) {
			currentFlower.setMultiplying(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("stemColour")) {
			currentVisParam.setStemColour(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("leafColour")) {
			currentVisParam.setLeafColour(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("aveLenFlower")) {
			currentVisParam.setAveLenFlower(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("aveLenFlowerMeasure")) {
			currentVisParam.setAveLenFlowerMeasure(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("tempreture")) {
			currentGrowTips.setTempreture(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("tempretureMeasure")) {
			currentGrowTips.setTempretureMeasure(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("lighting")) {
			currentGrowTips.setLighting(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("lightRequiring")) {
			currentGrowTips.setLightRequiring(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("watering")) {
			currentGrowTips.setWatering(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("wateringMeasure")) {
			currentGrowTips.setWateringMeasure(currentValue.toString());
		}


		if (qName.equalsIgnoreCase("flower")) {
			flowerList.add(currentFlower);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		currentValue.append(ch, start, length);
	}
}