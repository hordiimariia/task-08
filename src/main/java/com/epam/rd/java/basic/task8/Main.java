package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parser();

		// save
		String outputXmlFile = "output.dom.xml";
		File outXml = domController.toXML(domController.getFlowerList(), new File(outputXmlFile));

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		SAXController saxController = new SAXController(xmlFileName);
		saxParser.parse(xmlFileName, saxController);

		// save
		outputXmlFile = "output.sax.xml";
		outXml = domController.toXML(saxController.getFlowerList(), new File(outputXmlFile));
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parser();

		// save
		outputXmlFile = "output.stax.xml";
		outXml = domController.toXML(staxController.getFlowerList(), new File(outputXmlFile));
	}

}
