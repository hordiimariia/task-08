package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	Flower currentFlower;
	Flower.VisualParameters currentVisParam;
	Flower.GrowingTips currentGrowTips;
	List<Flower> flowerList = new ArrayList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getFlowerList() {
		return flowerList;
	}

	public void parser() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case "flower":
						currentFlower = new Flower();
						currentVisParam = currentFlower.new VisualParameters();
						currentGrowTips = currentFlower.new GrowingTips();

						currentFlower.setVisualParameters(currentVisParam);
						currentFlower.setGrowingTips(currentGrowTips);
						break;
					case "name":
						nextEvent = reader.nextEvent();
						currentFlower.setName(nextEvent.asCharacters().getData());
						break;
					case "soil":
						nextEvent = reader.nextEvent();
						currentFlower.setSoil(nextEvent.asCharacters().getData());
						break;
					case "origin":
						nextEvent = reader.nextEvent();
						currentFlower.setOrigin(nextEvent.asCharacters().getData());
						break;
					case "stemColour":
						nextEvent = reader.nextEvent();
						currentVisParam.setStemColour(nextEvent.asCharacters().getData());
						break;
					case "leafColour":
						nextEvent = reader.nextEvent();
						currentVisParam.setLeafColour(nextEvent.asCharacters().getData());
						break;
					case "aveLenFlower":
						nextEvent = reader.nextEvent();
						currentVisParam.setAveLenFlower(nextEvent.asCharacters().getData());

						Attribute lenMeasure = startElement.getAttributeByName(new QName("measure"));
						if (lenMeasure != null) {
							currentVisParam.setAveLenFlowerMeasure(lenMeasure.getValue());
						}
						break;
					case "tempreture":
						nextEvent = reader.nextEvent();
						currentGrowTips.setTempreture(nextEvent.asCharacters().getData());

						Attribute temprMeasure = startElement.getAttributeByName(new QName("measure"));
						if (temprMeasure != null) {
							currentGrowTips.setTempretureMeasure(temprMeasure.getValue());
						}
						break;
					case "lighting":
						Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
						if (lightRequiring != null) {
							currentGrowTips.setLightRequiring(lightRequiring.getValue());
						}
						break;
					case "watering":
						nextEvent = reader.nextEvent();
						currentGrowTips.setWatering(nextEvent.asCharacters().getData());

						Attribute waterMeasure = startElement.getAttributeByName(new QName("measure"));
						if (waterMeasure != null) {
							currentGrowTips.setWateringMeasure(waterMeasure.getValue());
						}
						break;
					case "multiplying":
						nextEvent = reader.nextEvent();
						currentFlower.setMultiplying(nextEvent.asCharacters().getData());
						break;
				}
			}
			if (nextEvent.isEndElement()) {
				EndElement endElement = nextEvent.asEndElement();
				if (endElement.getName().getLocalPart().equals("flower")) {
					flowerList.add(currentFlower);
				}
			}
		}
	}

	// PLACE YOUR CODE HERE

}