package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public class VisualParameters{
        private String stemColour;
        private String leafColour;
        private String aveLenFlower;
        private String aveLenFlowerMeasure;

        public VisualParameters() {
        }

        public VisualParameters(String stemColour, String leafColour, String aveLenFlower, String aveLenFlowerMeasure) {
            this.stemColour = stemColour;
            this.leafColour = leafColour;
            this.aveLenFlower = aveLenFlower;
            this.aveLenFlowerMeasure = aveLenFlowerMeasure;
        }

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public String getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(String aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }

        public String getAveLenFlowerMeasure() {
            return aveLenFlowerMeasure;
        }

        public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
            this.aveLenFlowerMeasure = aveLenFlowerMeasure;
        }
    }

    public class GrowingTips {
        private String tempreture;
        private String tempretureMeasure;
        private String lighting;
        private String lightRequiring;
        private String watering;
        private String wateringMeasure;

        public GrowingTips() {
        }

        public GrowingTips(String tempreture, String tempretureMeasure,
                           String lighting, String lightRequiring,
                           String watering, String wateringMeasure) {
            this.tempreture = tempreture;
            this.tempretureMeasure = tempretureMeasure;
            this.lightRequiring = lightRequiring;
            this.lighting = lighting;
            this.watering = watering;
            this.wateringMeasure = wateringMeasure;
        }

        public String getTempreture() {
            return tempreture;
        }

        public void setTempreture(String tempreture) {
            this.tempreture = tempreture;
        }

        public String getTempretureMeasure() {
            return tempretureMeasure;
        }

        public void setTempretureMeasure(String tempretureMeasure) {
            this.tempretureMeasure = tempretureMeasure;
        }

        public String getLighting() {
            return lighting;
        }

        public void setLighting(String lighting) {
            this.lighting = lighting;
        }

        public String getLightRequiring() {
            return lightRequiring;
        }

        public void setLightRequiring(String lightRequiring) {
            this.lightRequiring = lightRequiring;
        }

        public String getWatering() {
            return watering;
        }

        public void setWatering(String watering) {
            this.watering = watering;
        }

        public String getWateringMeasure() {
            return wateringMeasure;
        }

        public void setWateringMeasure(String wateringMeasure) {
            this.wateringMeasure = wateringMeasure;
        }
    }

    public Flower() {
    }

    public Flower(String name, String soil, String origin, String multiplying,
                  String stemColour, String leafColour, String  aveLenFlower, String aveLenFlowerMeasure,
                  String tempreture, String tempretureMeasure, String lighting, String lightRequiring,
                  String watering, String wateringMeasure) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.multiplying = multiplying;
        visualParameters = new VisualParameters(stemColour, leafColour, aveLenFlower, aveLenFlowerMeasure);
        growingTips = new GrowingTips(tempreture, tempretureMeasure, lighting, lightRequiring, watering, wateringMeasure);
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}
