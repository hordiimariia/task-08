package com.epam.rd.java.basic.task8.controller;



import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	List<Flower> flowerList = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getFlowerList() {
		return flowerList;
	}

	public void parser() throws ParserConfigurationException, IOException, SAXException {
		String name, soil, origin, multiplying,
				stemColour, leafColour, aveLenFlower, aveLenFlowerMeasure,
				tempreture, tempretureMeasure, lighting, lightRequiring, watering, wateringMeasure;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlFileName);
		document.getDocumentElement().normalize();
		NodeList nodeList = document.getElementsByTagName("flower");

		for (int i = 0; i < nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element =(Element) node;
				name = element.getElementsByTagName("name").item(0).getTextContent();
				soil = element.getElementsByTagName("soil").item(0).getTextContent();
				origin = element.getElementsByTagName("origin").item(0).getTextContent();
				multiplying = element.getElementsByTagName("multiplying").item(0).getTextContent();
				stemColour = element.getElementsByTagName("stemColour").item(0).getTextContent();
				leafColour = element.getElementsByTagName("leafColour").item(0).getTextContent();
				aveLenFlower = element.getElementsByTagName("aveLenFlower").item(0).getTextContent();
				aveLenFlowerMeasure = element.getElementsByTagName("aveLenFlower").item(0).getAttributes().item(0).getTextContent();
				tempreture = element.getElementsByTagName("tempreture").item(0).getTextContent();
				tempretureMeasure = element.getElementsByTagName("tempreture").item(0).getAttributes().item(0).getTextContent();

				lighting = element.getElementsByTagName("lighting").item(0).getTextContent();
				lightRequiring = element.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent();


				watering = element.getElementsByTagName("watering").item(0).getTextContent();
				wateringMeasure = element.getElementsByTagName("watering").item(0).getAttributes().item(0).getTextContent();

				Flower flower = new Flower(name, soil, origin, multiplying,
						stemColour, leafColour, aveLenFlower, aveLenFlowerMeasure,
						tempreture, tempretureMeasure, lighting, lightRequiring, watering, wateringMeasure);

				flowerList.add(flower);
			}
		}
	}

	public File toXML(List<Flower> flowerList, File file) throws ParserConfigurationException, TransformerException {
		File xmlFile = file;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();

		Element rootElement = document.createElementNS("http://www.nure.ua", "flowers");
		document.appendChild(rootElement);

		for (Flower f: flowerList){
			rootElement.appendChild(getFlower(document,
					f.getName(), f.getSoil(), f.getOrigin(), f.getMultiplying(),
					f.getVisualParameters().getStemColour(), f.getVisualParameters().getLeafColour(),
					f.getVisualParameters().getAveLenFlower(), f.getVisualParameters().getAveLenFlowerMeasure(),
					f.getGrowingTips().getTempreture(), f.getGrowingTips().getTempretureMeasure(),
					f.getGrowingTips().getLighting(), f.getGrowingTips().getLightRequiring(),
					f.getGrowingTips().getWatering(), f.getGrowingTips().getWateringMeasure()));
		}

			//создаем объект TransformerFactory для печати в консоль
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			// для красивого вывода в консоль
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(document);

			//печатаем в консоль или файл
			StreamResult console = new StreamResult(System.out);
			StreamResult f = new StreamResult(xmlFile);

			//записываем данные
			transformer.transform(source, f);
			return xmlFile;
	}

	// метод для создания нового узла XML-файла
	private static Node getFlower(Document document,
								  String name, String soil, String origin, String multiplying,
								  String stemColour, String leafColour, String  aveLenFlower, String aveLenFlowerMeasure,
								  String tempreture, String tempretureMeasure, String lighting, String lightRequiring,
								  String watering, String wateringMeasure) {

		Element flower = document.createElement("flower");
		Element visualParameters = document.createElement("visualParameters");
		Element growingTips = document.createElement("growingTips");

		// создаем элемент name
		flower.appendChild(getFlowerElements(document, flower, "name", name, null));
		flower.appendChild(getFlowerElements(document, flower, "soil", soil, null));
		flower.appendChild(getFlowerElements(document, flower, "origin", origin, null));

		visualParameters.appendChild(getFlowerElements(document, flower, "stemColour", stemColour, null));
		visualParameters.appendChild(getFlowerElements(document, flower, "leafColour", leafColour, null));
		visualParameters.appendChild(getFlowerElements(document, flower, "aveLenFlower", aveLenFlower, "measure="+aveLenFlowerMeasure));

		flower.appendChild(visualParameters);

		growingTips.appendChild(getFlowerElements(document, flower, "tempreture", tempreture, "measure="+tempretureMeasure));
		growingTips.appendChild(getFlowerElements(document, flower, "lighting", lighting, "lightRequiring="+lightRequiring));
		growingTips.appendChild(getFlowerElements(document, flower, "watering", watering, "measure="+wateringMeasure));

		flower.appendChild(growingTips);

		flower.appendChild(getFlowerElements(document, flower, "multiplying", multiplying, null));


		return flower;
	}


	// утилитный метод для создание нового узла XML-файла
	private static Node getFlowerElements(Document doc, Element element, String name, String value, String attribute) {
		Element node = doc.createElement(name);
		if(attribute != null) {
			String[] arr = attribute.split("=");
			node.setAttribute(arr[0], arr[1]);
		}
		node.appendChild(doc.createTextNode(value));
		return node;
	}

}
